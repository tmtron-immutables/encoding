package com.tmtron.immutables.date;

import org.immutables.encode.Encoding;

import java.util.Date;

@Encoding
public class DateEncoding {

    @Encoding.Impl
    private long dateAsLong;

    @Encoding.Expose
    Date getDate() {
        return new java.util.Date(dateAsLong);
    }

    @Encoding.Of
    static Long init(Date date) {
        return date.getTime();
    }

    public boolean equals(DateEncoding o) {
        return dateAsLong == o.dateAsLong;
    }

    @Override
    public int hashCode() {
        return (int) (dateAsLong ^ (dateAsLong >>> 32));
    }

    @Override
    public String toString() {
        return "DateEncoding{" +
                "dateAsLong=" + dateAsLong +
                '}';
    }

    @Encoding.Builder
    static class Builder {

        private long buildValue = 0L;

        @Encoding.Init
        @Encoding.Copy
        public void set(Date date) {
            this.buildValue = date.getTime();
        }

        @Encoding.Build
        long build() {
            return buildValue;
        }
    }
}
