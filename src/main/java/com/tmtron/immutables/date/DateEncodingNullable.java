package com.tmtron.immutables.date;

import org.immutables.encode.Encoding;

import java.util.Date;

@Encoding
public class DateEncodingNullable {

    @Encoding.Impl
    private Long dateAsLong;

    @Encoding.Expose
    Date getDate() {
        if (dateAsLong == null) {
            return null;
        } else {
            return new java.util.Date(dateAsLong);
        }
    }

    @Encoding.Of
    static Long init(Date date) {
        if (date != null) {
            return date.getTime();
        } else {
            return null;
        }
    }

    public boolean equals(DateEncodingNullable o) {
        return o != null
                && (dateAsLong != null ? dateAsLong.equals(o.dateAsLong) : o.dateAsLong == null);
    }

    @Override
    public int hashCode() {
        return dateAsLong != null ? dateAsLong.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "DateEncodingNullable{" +
                "dateAsLong=" + dateAsLong +
                '}';
    }

    @Encoding.Builder
    static class Builder {

        private Long buildValue = null;

        @Encoding.Init
        @Encoding.Copy
        public void set(Date date) {
            if (date == null) {
                this.buildValue = null;
            } else {
                this.buildValue = date.getTime();
            }
        }

        @Encoding.Build
        Long build() {
            return buildValue;
        }
    }
}
