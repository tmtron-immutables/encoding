package com.tmtron.immutables.test;

import com.tmtron.immutables.datadef.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

public class DataWrapperTest {

    @Test
    public void testDataObjectIsMutable() {
        Date date1 = new Date();
        DataWrapper dataObject = ImmutableDataWrapper.builder()
                .name("test")
                .creationDate(new ImmutableDateWrapper(date1))
                .build();
        Assert.assertEquals(date1, dataObject.creationDate().getDate());
        Date date2 = dataObject.creationDate().getDate();
        date1.setTime(date1.getTime() + 100);
        Assert.assertNotEquals(date1, dataObject.creationDate().getDate());
        Assert.assertEquals(date2, dataObject.creationDate().getDate());
    }

}
