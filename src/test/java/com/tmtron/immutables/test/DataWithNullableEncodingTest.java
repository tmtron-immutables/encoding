package com.tmtron.immutables.test;

import com.tmtron.immutables.datadef.DataWithNullableEncoding;
import com.tmtron.immutables.datadef.ImmutableDataWithNullableEncoding;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

public class DataWithNullableEncodingTest {

    @Test
    public void testDataObjectImmutable() {
        Date date1 = new Date();
        DataWithNullableEncoding dataObject = ImmutableDataWithNullableEncoding.builder()
                .name("test")
                .creationDate(date1)
                .build();
        Assert.assertEquals(date1.getTime(), dataObject.creationDate().getTime());
        date1.setTime(date1.getTime() + 100);
        // changing the source-date must not change the date in the immutable object
        Assert.assertNotEquals(date1.getTime(), dataObject.creationDate().getTime());
    }


    @Test
    public void testNull() {
        DataWithNullableEncoding dataObject = ImmutableDataWithNullableEncoding.builder()
                .name("test")
                .creationDate(null)
                .build();
        // we can set the date to null and will get back null without NPEs
        Assert.assertNull(dataObject.creationDate());
    }

}
