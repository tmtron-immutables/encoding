package com.tmtron.immutables.test;

import com.tmtron.immutables.datadef.DataNoEncoding;
import com.tmtron.immutables.datadef.DataProtected;
import com.tmtron.immutables.datadef.ImmutableDataNoEncoding;
import com.tmtron.immutables.datadef.ImmutableDataProtected;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

public class DataProtectedTest {

    @Test
    public void testDataObjectIsMutable() {
        Date date1 = new Date();
        DataProtected dataObject = ImmutableDataProtected.builder()
                .name("test")
                .creationDate(date1)
                .build();
        /* note: dataObject.creationTime() is not accessible, because it is protected
         */

        Assert.assertEquals(date1.getTime(), dataObject.getCreationDate().getTime());
        date1.setTime(date1.getTime() + 100);
        /* we have just changed our dataObject!
         * since we added a mutable Date instance to the dataObject it is NOT immutable
         */
        Assert.assertEquals(date1.getTime(), dataObject.getCreationDate().getTime());
    }

}
