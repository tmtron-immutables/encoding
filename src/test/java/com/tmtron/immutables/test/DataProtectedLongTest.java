package com.tmtron.immutables.test;

import com.tmtron.immutables.datadef.DataProtected;
import com.tmtron.immutables.datadef.DataProtectedLong;
import com.tmtron.immutables.datadef.ImmutableDataProtected;
import com.tmtron.immutables.datadef.ImmutableDataProtectedLong;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

public class DataProtectedLongTest {

    @Test
    public void testDataObjectIsMutable() {
        Date date1 = new Date();
        DataProtectedLong dataObject = ImmutableDataProtectedLong.builder()
                .name("test")
                .creationDateAsLong(date1.getTime())
                .build();
        Assert.assertEquals(date1.getTime(), dataObject.getCreationDate().getTime());
        Assert.assertEquals(date1.getTime(), dataObject.creationDateAsLong());
        date1.setTime(date1.getTime() + 100);
        Assert.assertNotEquals(date1.getTime(), dataObject.getCreationDate().getTime());
        Assert.assertNotEquals(date1.getTime(), dataObject.creationDateAsLong());
    }

}
