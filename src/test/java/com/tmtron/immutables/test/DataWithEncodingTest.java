package com.tmtron.immutables.test;

import com.tmtron.immutables.datadef.DataWithEncoding;
import com.tmtron.immutables.datadef.ImmutableDataWithEncoding;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

public class DataWithEncodingTest {

    @Test
    public void testDataObjectImmutable() {
        Date date1 = new Date();
        DataWithEncoding dataObject = ImmutableDataWithEncoding.builder()
                .name("test")
                .creationDate(date1)
                .build();
        Assert.assertEquals(date1.getTime(), dataObject.creationDate().getTime());
        date1.setTime(date1.getTime() + 100);
        // changing the source-date must not change the date in the immutable object
        Assert.assertNotEquals(date1.getTime(), dataObject.creationDate().getTime());
    }


    @Test(expected = NullPointerException.class)
    public void testNull() {
        ImmutableDataWithEncoding.builder()
                .name("test")
                .creationDate(null)
                .build();
    }

}
