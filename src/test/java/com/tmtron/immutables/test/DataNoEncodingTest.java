package com.tmtron.immutables.test;

import com.tmtron.immutables.datadef.DataNoEncoding;
import com.tmtron.immutables.datadef.ImmutableDataNoEncoding;
import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

public class DataNoEncodingTest {

    @Test
    public void testDataObjectIsMutable() {
        Date date1 = new Date();
        DataNoEncoding dataObject = ImmutableDataNoEncoding.builder()
                .name("test")
                .creationDate(date1)
                .build();
        Assert.assertEquals(date1.getTime(), dataObject.creationDate().getTime());
        date1.setTime(date1.getTime() + 100);
        /* we have just changed our dataObject!
         * since we added a mutable Date instance to the dataObject it is NOT immutable
         */
        Assert.assertEquals(date1.getTime(), dataObject.creationDate().getTime());
    }

    @Test(expected = NullPointerException.class)
    public void testNull() {
        ImmutableDataNoEncoding.builder()
                .name("test")
                .creationDate(null)
                .build();
    }

}
