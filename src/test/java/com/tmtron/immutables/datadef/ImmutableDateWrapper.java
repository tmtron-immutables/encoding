package com.tmtron.immutables.datadef;

import java.util.Date;

public class ImmutableDateWrapper {

    private long dateAsLong;

    public ImmutableDateWrapper(Date date) {
        this.dateAsLong = date.getTime();
    }

    public Date getDate() {
        return new Date(dateAsLong);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ImmutableDateWrapper that = (ImmutableDateWrapper) o;

        return dateAsLong == that.dateAsLong;
    }

    @Override
    public int hashCode() {
        return (int) (dateAsLong ^ (dateAsLong >>> 32));
    }

    @Override
    public String toString() {
        return "ImmutableDateWrapper{" +
                "dateAsLong=" + dateAsLong +
                '}';
    }
}
