package com.tmtron.immutables.datadef;

import com.tmtron.immutables.date.DateEncodingNullableEnabled;
import org.immutables.value.Value;

import java.util.Date;

@Value.Immutable
@DateEncodingNullableEnabled
public interface DataWithNullableEncoding {
    String name();

    Date creationDate();
}
