package com.tmtron.immutables.datadef;

import org.immutables.value.Value;

import java.util.Date;

@Value.Immutable
public abstract class DataProtected {
    abstract String name();

    protected abstract Date creationDate();

    public Date getCreationDate() {
        return new Date(creationDate().getTime());
    }
}
