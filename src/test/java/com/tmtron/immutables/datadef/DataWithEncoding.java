package com.tmtron.immutables.datadef;

import com.tmtron.immutables.date.DateEncodingEnabled;
import org.immutables.value.Value;

import java.util.Date;

@Value.Immutable
@DateEncodingEnabled
public interface DataWithEncoding {
    String name();

    Date creationDate();
}
