package com.tmtron.immutables.datadef;

import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

public class DataProtectedSamePackageTest {

    @Test
    public void testDataObjectIsMutable() {
        Date date1 = new Date();
        DataProtected dataObject = ImmutableDataProtected.builder()
                .name("test")
                .creationDate(date1)
                .build();
        Assert.assertEquals(date1.getTime(), dataObject.getCreationDate().getTime());
        date1.setTime(date1.getTime() + 100);
        /* we have just changed our dataObject!
         * since we added a mutable Date instance to the dataObject it is NOT immutable
         */
        Assert.assertEquals(date1.getTime(), dataObject.getCreationDate().getTime());

        // note: since we are in the same package, we can access the protected attribute!
        dataObject.creationDate().setTime(100);
        Assert.assertEquals(100, dataObject.getCreationDate().getTime());
    }

}
