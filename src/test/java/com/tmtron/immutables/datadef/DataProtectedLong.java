package com.tmtron.immutables.datadef;

import org.immutables.value.Value;

import java.util.Date;

@Value.Immutable
public abstract class DataProtectedLong {
    abstract String name();

    public abstract long creationDateAsLong();

    // convenience accessor
    public Date getCreationDate() {
        return new Date(creationDateAsLong());
    }
}
