package com.tmtron.immutables.datadef;

import org.immutables.value.Value;

import java.util.Date;

@Value.Immutable
public interface DataWrapper {
    String name();

    ImmutableDateWrapper creationDate();
}
