# Dealing with mutable attributes

For general information please read the main [README.md file](../README.md)

## Ignore it
If you only have a simple test project, this might be okay for you.  
But then you may not need to use [Immutables](http://immutables.github.io) anyway, right?

Ignoring the mutable attribute is of course a risk and can lead to unexpected behaviour and bugs that are hard to find. 

## Protected Access
You can make the attribute protected and provide a getter which will always return a fresh copy.  
Inspired by this [Stackoverflow answer](http://stackoverflow.com/a/42346768/6287240) from [henrik](http://stackoverflow.com/users/13075/henrik)

```java
    @Value.Immutable
    public abstract class ValueObject {
        protected abstract Date creationDate();
    
        public Date getCreationDate() {
            return new Date(creationDate().getTime());
        }
    }
```

This should be good enough for many use-cases.  
Note: a user could of course create a java class in the same package as your ValueObject 
and then the protected member `creationDate` can be accessed and changed.

## Replace the mutable Class
Simply replace the mutable class with an immutable equivalent.  
For example, instead of the mutable [java.util.Date](http://docs.oracle.com/javase/6/docs/api/java/util/Date.html) you could use any of these:

* the immutable Java8 library: [java.time](https://docs.oracle.com/javase/8/docs/api/java/time/package-summary.html)
* immutable classes from the [Joda-Time library](http://www.joda.org/joda-time/)
* immutable classes from [The ThreeTen project](http://www.threeten.org/)

This may of course not always be an option, depending on your project constraints.
  
## Create an immutable wrapper class
The idea is that you create an immutable wrapper class that stores only the relevant data of the original class,
and then you only use this immutable wrapper class in your ValueObject.

Example:

```java
  public class ImmutableDateWrapper {
  
      private long dateAsLong;
  
      public ImmutableDateWrapper(Date date) {
          this.dateAsLong = date.getTime();
      }
  
      public Date getDate() {
          return new Date(dateAsLong);
      }
      
      // NOTE: you may also want to implement equals(), hashCode(), toString()
  }
```

The drawback of this approach is, that it is quite some work to create the wrapper-class
and to use it in the builder/getters:
  
```java  
    ImmutableValueObject.builder()
            .creationDate(new ImmutableDateWrapper(date1))
            .build();
```

## Manually store only the immutable state
Instead of using the mutable class in your ValueObject, store only the immutable parts.

Example:

```java
    @Value.Immutable
    public abstract class ValueObject {
        public abstract long creationDateAsLong();
    
        // convenience accessor
        public Date getCreationDate() {
            return new Date(creationDateAsLong());
        }
    }
```

Since we only store the primitive long, the ValueObject is immutable.  
We also provide a convenience accessor which will create a new Data object.
 
The drawback of this approach is that you cannot simply pass the Date instance
to the builder, but you must provide the immutable long:

```java
    ImmutableValueObject.builder()
      .creationDateAsLong(date1.getTime())
      .build();
```

## Use custom type encoding annotation
This is what the [tmtron-immutables encoding project](https://gitlab.com/tmtron-immutables/encoding) is all about.

Please refer to the main [README.md file](../README.md)
