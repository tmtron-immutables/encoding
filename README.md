# immutables encodings
This project contains [immutable encodings](http://immutables.github.io/encoding.html) which
can be used to enforce immutability for mutable attributes when using
the [Immutables java library](http://immutables.github.io).  
Currently this project only contains an encoding for the
 [java.util.Date](http://docs.oracle.com/javase/6/docs/api/java/util/Date.html) class.

**TL;TR;**  
If you must use [java.util.Date](http://docs.oracle.com/javase/6/docs/api/java/util/Date.html) 
in your immutable ValueObject, then this project can help you a great deal.  
You should **not** use this project if you can use alternatives: 
see [Dealing with mutable attributes](docs/DealingWithMutableAttributes.md).

## Quick start
### Gradle setup
The library is available on 
[MavenCentral](https://search.maven.org/#search%7Cga%7C1%7Ccom.tmtron), 
[JCenter](https://bintray.com/bintray/jcenter) and on our 
[company bintray repository](https://dl.bintray.com/tmtron/maven).  
so you can choose any of the 3 repositories in your `build.gradle` file:
```gradle
repositories {
    mavenCentral()
    jcenter()
    maven { url 'https://dl.bintray.com/tmtron/maven' }
}
dependencies {
    compileOnly 'com.tmtron.immutables:encoding:1.0.1'
}
```
Note: since the library only contains annotations and annotation processors, 
you can add it to the `compileOnly` configuration.

### Usage
Create a `package-info.java` file in the top-level package of your application,
 so that the Date attributes of all your ValueObjects will be processed:
```java
    @DateEncodingEnabled
    package com.tmtron.immutables.packagewide;
          
    import com.tmtron.immutables.date.DateEncodingEnabled;
```

## Problem scope

[Immutables](http://immutables.github.io) is a great library to create value objects.
 
But sometimes you have mutable objects that you want to use as attributes of your value objects.    
When you simply use the mutable object as an attribute, everything will be created as usual, but your
ImmutableValueObject will now be mutable!

Example:

```java
    @Value.Immutable
    public interface ValueObject {
        Date creationDate();
    }
```

will create this "immutable" value object:
 
```java
     public final class ImmutableValueObject implements ValueObject {
       private final Date creationDate;
     
       @Override
       public Date creationDate() {
         return creationDate;
       }
```

You can see that you can directly access the [java.util.Date](http://docs.oracle.com/javase/6/docs/api/java/util/Date.html)
 field and you may change it: so your value object is not that immutable after all.

All different ways to deal with this problem are described in 
[Dealing with mutable attributes](docs/DealingWithMutableAttributes.md).

If you cannot use any of the alternatives and you must use 
[java.util.Date](http://docs.oracle.com/javase/6/docs/api/java/util/Date.html)
in your project, then this project can help you a great deal. 


## Enabling the custom type encoding annotation
To use the custom type encoding annotation of this project, you can apply it 
 to all your ValueObject classes/interfaces directly or better for a
 whole java-package (recommended).

When the annotation is applied, you can directly use the mutable 
 [java.util.Date](http://docs.oracle.com/javase/6/docs/api/java/util/Date.html)
 class as your attribute type and the annotation processor will take care of the immutability.
 
Example:

```java
    @Value.Immutable
    @DateEncodingEnabled
    public interface ValueObject {
        Date creationDate();
    }
```

Generated class:

```java
    public final class ImmutableValueObject implements ValueObject {
      private final long creationDate;
    
      @Override
      public Date creationDate() {
        return new Date(creationDate);
      }
    ...
      
      public static final class Builder {
        private long creationDate_buildValue = 0L;

        public Builder creationDate(Date date) {
          this.creationDate_buildValue = date.getTime();
          return this;
        }
    }
```

Notes:
* the ImmutableValueObject only stores a primitive long value and that the builder will read the long value 
from the provided date object
* you can directly use the [java.util.Date](http://docs.oracle.com/javase/6/docs/api/java/util/Date.html)
 instances in the builder and accessor

Example usage:

```java
    Date date1 = new java.util.Date();
    ValueObject valueObject = ImmutableValueObject.builder()
        .creationDate(date1)
        .build();
      
    date1.setTime(date1.getTime() + 100);
    // changing the source-date will not change the date in the immutable valueObject
    Assert.assertNotEquals(date1.getTime(), valueObject.creationDate().getTime());
```

### Package configuration

When you want to apply the annotation to a whole package (and sub-packages),
just create a `package-info.java` file with this content:

```java
    @DateEncodingEnabled
    package com.tmtron.immutables.packagewide;
          
    import com.tmtron.immutables.date.DateEncodingEnabled;
```

Related links:

- [Immutables project](http://immutables.github.io/)
- [Immutables encoding docs](http://immutables.github.io/encoding.html)
- [Immutables question #557: how to handle mutable attributes](https://github.com/immutables/immutables/issues/557)
- maven based encoding example: https://github.com/immutables/samples
- mini test-project using gradle: https://gitlab.com/tmtron-immutables/encoding-use
- [Stackoverflow: Mutable items in Immutable](http://stackoverflow.com/questions/42345684/mutable-items-in-immutable/42346768#42346768)
- Annotation processors in IntelliJ
  - [gradle-apt-plugin issue#35](https://github.com/tbroyer/gradle-apt-plugin/issues/35#issuecomment-282482795)
  - [Stackoverflow: Annotation Processor in IntelliJ and Gradle](http://stackoverflow.com/questions/42441844/annotation-processor-in-intellij-and-gradle)







